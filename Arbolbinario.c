#include <stdio.h>
#include <stdlib.h>

/*
 * Andres Lopez 
 */

struct arbol{
    int valor;
    struct nodoArbol*left;
    struct nodoARbol*right;
};
struct arbol*raiz=NULL;

void ingresarNodo(int x){

    struct arbol*nuevo;
    nuevo = malloc(sizeof(struct arbol));
    nuevo->valor = x;
    nuevo->left = NULL;
    nuevo->right =NULL;
    if(raiz == NULL)
        raiz= nuevo;
    else{
    
        struct arbol *anterior, *reco;
        anterior = NULL;
        reco = raiz;
        while (reco != NULL)
        {
            anterior = reco;
            if (x < reco->valor)
                reco = reco->left;
            else
                reco = reco->right;
        }
        if (x < anterior->valor)
            anterior->left = nuevo;
        else
            anterior->right = nuevo;
    }
    
}



void postOrden(struct arbol *recorrer)
{
    if (recorrer != NULL)
    {
        postOrden(recorrer->left);
        postOrden(recorrer->right);
        printf("%i-",recorrer->valor);
    }
}



int main(int argc, char** argv) {
    
    ingresarNodo(13);
    ingresarNodo(12);
    ingresarNodo(67);
    ingresarNodo(9);
    ingresarNodo(4);
    postOrden(raiz);
    
    return (EXIT_SUCCESS);
}

